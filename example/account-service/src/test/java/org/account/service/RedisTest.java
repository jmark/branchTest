/**
 * Copyright (c) 2015 Sohu. All Rights Reserved
 */
package org.account.service;

import org.junit.Test;

import redis.clients.jedis.Jedis;

/**
 * <p>
 * Description: Redis测试
 * </p>
 * @author majintao
 * @version 1.0
 * @Date 2015年6月25日
 */
public class RedisTest {

    @Test
    public void test() {
        Jedis jedis = new Jedis("localhost");
        jedis.set("jkey", "HelloRedis");
        System.out.println(jedis.get("jkey"));
    }
}
