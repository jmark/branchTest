/**
 * Copyright (c) 2015 Sohu. All Rights Reserved
 */
package org.account.service;

import org.junit.Test;
import org.nutz.ssdb4j.SSDBs;
import org.nutz.ssdb4j.impl.SimpleClient;
import org.nutz.ssdb4j.spi.SSDB;

/**
 * <p>
 * Description: 
 * </p>
 * @author majintao
 * @version 1.0
 * @Date 2015年6月24日
 */
public class SsdbTest {
    
    @Test
    public void ssdb() {
        SSDB ssdb = new SimpleClient("14.17.117.195",8888,2000);
        ssdb.set("test1", "jintaoma");
        System.out.println(ssdb.get("test1"));
    }
    
}
