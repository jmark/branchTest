/**
 * Copyright (c) 2015 Sohu. All Rights Reserved
 */
package org.account.springredis;

import javax.annotation.Resource;

import org.account.dao.IUserDao;
import org.account.model.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


/**
 * <p>
 * Description: 
 * </p>
 * @author majintao
 * @version 1.0
 * @Date 2015年6月25日
 */
@RunWith(SpringJUnit4ClassRunner.class) 
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})  
public class SpringRedisTest {
    
    @Resource
    private IUserDao userDao;
    
    private static String userId = "userId";
    
    @Test
    public void testAdd() {
       User user = new User();
       user.setId(userId);
       user.setName("jintaoma");
       user.setPassword("jintaoma");
       userDao.add(user);
    }
    
    @Test
    public void testGet() {
        User user = userDao.get(userId);
        System.out.println(user.toString());
    }

    
    
    public IUserDao getUserDao() {
        return userDao;
    }

    public void setUserDao(IUserDao userDao) {
        this.userDao = userDao;
    }
}
