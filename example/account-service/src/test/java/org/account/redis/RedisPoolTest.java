/**
 * Copyright (c) 2015 Sohu. All Rights Reserved
 */
package org.account.redis;

import java.util.ResourceBundle;

import org.junit.Test;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * <p>
 * Description: 测试Redis线程池配置连接
 * </p>
 * @author majintao
 * @version 1.0
 * @Date 2015年6月25日
 */
public class RedisPoolTest {
    
    private static JedisPool jpool;
    
    static {
        ResourceBundle bundle = ResourceBundle.getBundle("JedisPool.properties");
        JedisPoolConfig config = new JedisPoolConfig();
        
        config.setMaxIdle(Integer.valueOf(bundle  
                .getString("redis.pool.maxActive")));  
        config.setMaxIdle(Integer.valueOf(bundle  
                .getString("redis.pool.maxIdle")));  
        config.setMaxWaitMillis(Long.valueOf(bundle.getString("redis.pool.maxWait")));  
        config.setTestOnBorrow(Boolean.valueOf(bundle  
                .getString("redis.pool.testOnBorrow")));    
        config.setTestOnReturn(Boolean.valueOf(bundle  
                .getString("redis.pool.testOnReturn"))); 
        
        String ip = bundle.getString("redis.ip");
        int port = Integer.valueOf(bundle.getString("redis.port"));
        jpool = new JedisPool(config,ip,port);
    }
    
    
    @Test
    public void test() {
        
     // 从池中获取一个Jedis对象  
        Jedis jedis = jpool.getResource();  
        String keys = "name";  
          
        // 删数据  
        jedis.del(keys);  
        // 存数据  
        jedis.set(keys, "snowolf");  
        // 取数据  
        String value = jedis.get(keys);  
          
        System.out.println(value);  
          
        // 释放对象池  
        jpool.returnResource(jedis);  

    }
}
