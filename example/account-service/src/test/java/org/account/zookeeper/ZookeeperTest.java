/**
 * Copyright (c) 2015 Sohu. All Rights Reserved
 */
package org.account.zookeeper;

import java.io.IOException;

import org.apache.zookeeper.KeeperException;
import org.junit.Test;

/**
 * <p>
 * Description: 
 * </p>
 * @author majintao
 * @version 1.0
 * @Date 2015年6月25日
 */
public class ZookeeperTest {
    
    @Test
    public void test() throws IOException, InterruptedException, KeeperException {
        ZooKeeperWatcher zw1 = new ZooKeeperWatcher();  
        zw1.connect("127.0.0.1", "/root1");  
        ZooKeeperWatcher zw2 = new ZooKeeperWatcher();  
        zw2.connect("127.0.0.1", "/root1");  
        new Thread(zw1).start();  
        new Thread(zw2).start();
        
        System.out.println("###更新配置###");
        ConfigCenter cc = new ConfigCenter("127.0.0.1","/root1");  
        cc.updateConfig("a");  
        cc.updateConfig("b");  
        cc.updateConfig("c");  
        cc.updateConfig("d");  
    }
}
